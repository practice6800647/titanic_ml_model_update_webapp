import logging

from sklearn.linear_model import LogisticRegression
from sklearn import metrics

from .data import TitanicDatasetLoader
logger = logging.getLogger("titanicml")


def _eval_metrics(x, y, model: LogisticRegression):
    predictions = model.predict(x)
    y_score = model.predict_proba(x)[:, 1]
    precision = metrics.precision_score(y, predictions)
    recall = metrics.recall_score(y, predictions)
    f1_score = metrics.f1_score(y, predictions)
    average_precision = metrics.average_precision_score(y, y_score)
    accuracy = metrics.accuracy_score(y, predictions)
    pr_curve = metrics.precision_recall_curve(y, y_score)
    return dict(accuracy=accuracy,
                precision=precision,
                recall=recall,
                f1_score=f1_score,
                average_precision=average_precision,
                pr_curve=dict(precision=list(pr_curve[0]),
                              recall=list(pr_curve[1]),
                              thresholds=list(pr_curve[2]),
                              )
                )


class TitanicLogisticModel:
    def __init__(self):
        self.model = LogisticRegression()

    def train(self, dataset_loader: TitanicDatasetLoader):
        _dataset = dataset_loader.get_dataset()
        self.model_old = self.model
        self.model = LogisticRegression()
        logger.debug("train data size: %d",
                     len(_dataset["x_train"]))
        self.model.fit(_dataset["x_train"],
                       _dataset["y_train"])
    
    def eval_metrics(self, dataset_loader: TitanicDatasetLoader):
        _dataset = dataset_loader.get_dataset()
        metrics = _eval_metrics(_dataset["x_test"], _dataset["y_test"],
                                self.model)
        self.metrics=dict(model=dict(test=metrics))
        if not dataset_loader.is_dataset_updated():
            logger.info("finish train. metrics: %s",
                        self.metrics)
            return self.metrics

        _dataset_old = dataset_loader.get_dataset_old()
        _dataset_add = dataset_loader.get_dataset_add()
        self.metrics["model"].update(
            test_old=_eval_metrics(_dataset_old["x_test"],
                                   _dataset_old["y_test"],
                                   self.model),
            test_add=_eval_metrics(_dataset_add["x_test"],
                                   _dataset_add["y_test"],
                                   self.model)
        )
        self.metrics["model_old"] = dict(
            test=_eval_metrics(_dataset["x_test"],
                               _dataset["y_test"],
                               self.model_old),
            test_old=_eval_metrics(_dataset_old["x_test"],
                                   _dataset_old["y_test"],
                                   self.model_old),
            test_add=_eval_metrics(_dataset_add["x_test"],
                                   _dataset_add["y_test"],
                                   self.model_old)
        )
        logger.debug("model id: %d, old model id: %d",
                     id(self.model), id(self.model_old))
        return self.metrics


    def predict(self, x):
        return self.model.predict(x)

        

