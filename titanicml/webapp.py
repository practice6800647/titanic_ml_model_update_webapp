import logging
import pandas as pd

from fastapi import FastAPI
from pydantic import BaseModel

from .modeling import TitanicLogisticModel

from .data import TitanicDatasetLoader

logger = logging.getLogger("titanicml")
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch.setFormatter(formatter)

logger.addHandler(ch)

titanic_url = "https://raw.githubusercontent.com/datasciencedojo/datasets/master/titanic.csv"
_data = pd.read_csv(titanic_url)
_data = _data[['Survived', 'Pclass', 'Sex', 'Age', 'Fare']]
_data['Sex'] = _data['Sex'].map({'male': 0, 'female': 1})
_data = _data.dropna()
original_data = (_data
                 .sample(frac=1, random_state=342)
                 .iloc[:len(_data)//2]
                 )

dataset_loader = TitanicDatasetLoader(original_data)
model = TitanicLogisticModel()

def init():
    logger.info("init server")
    model.train(dataset_loader)
    model.eval_metrics(dataset_loader)

    
class TitanicUpdateData(BaseModel):
    Survived: list[int] = [0, 1]
    Pclass: list[int] = [3, 1]
    Sex: list[int] = [0, 1]
    Age: list[float] = [22.0, 50.0]
    Fare: list[float] = [7.25, 30.0]


class TitanicPredictData(BaseModel):
    # Survived: list[int] = [0, 1]
    Pclass: list[int] = [3, 1]
    Sex: list[int] = [0, 1]
    Age: list[float] = [22.0, 50.0]
    Fare: list[float] = [7.25, 30.0]


app = FastAPI()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/titanic/predict")
async def predict(data: TitanicPredictData):
    df = pd.DataFrame(data.model_dump())
    result = model.predict(df)
    result = dict(Survived=result.tolist())
    logger.debug(result)
    return result


@app.post("/titanic/update")
async def update(data: TitanicUpdateData):
    df = pd.DataFrame(data.model_dump())
    dataset_loader.update_dataset(df)
    logger.info("Retrain model")
    model.train(dataset_loader)
    metrics = model.eval_metrics(dataset_loader)

    return metrics


init()
