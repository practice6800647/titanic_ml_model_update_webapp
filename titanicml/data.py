from dataclasses import dataclass
import logging
import pandas as pd
from sklearn.model_selection import train_test_split

logger = logging.getLogger("titanicml")


def _concat(df, df_add):
    return pd.concat([df, df_add], ignore_index=True)


class TitanicDatasetLoader:
    test_size = 0.2
    random_state = 42

    def __init__(self, df):
        logger.info(df)
        self._dataset = self._split_data(df)
        self._dataset_add = None
        self._dataset_old = None

    @classmethod
    def _split_data(cls, df):
        x = df.drop('Survived', axis=1)
        y = df['Survived']
        x_train, x_test, y_train, y_test = train_test_split(
            x, y, test_size=cls.test_size, random_state=cls.random_state)
        return dict(x_train=x_train, x_test=x_test,
                    y_train=y_train, y_test=y_test)

    def get_dataset(self):
        return self._dataset

    def get_dataset_old(self):
        return self._dataset_old

    def get_dataset_add(self):
        return self._dataset_add

    def is_dataset_updated(self):
        return self._dataset_add is not None and self._dataset_old is not None

    def update_dataset(self, df):
        self._dataset_add = self._split_data(df)
        self._dataset_old = self._dataset
        self._dataset = dict(
            x_train=_concat(self._dataset["x_train"], self._dataset_add["x_train"]),
            y_train=_concat(self._dataset["y_train"], self._dataset_add["y_train"]),
            x_test=_concat(self._dataset["x_test"], self._dataset_add["x_test"]),
            y_test=_concat(self._dataset["y_test"], self._dataset_add["y_test"]),
        )
        logger.info("update dataset: train: %d->%d, test: %d->%d",
                    self._dataset_old["y_train"].size, self._dataset["y_train"].size,
                    self._dataset_old["y_test"].size, self._dataset["y_test"].size)

