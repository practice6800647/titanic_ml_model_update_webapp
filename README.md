# Titanic号生還結果予測モデルの推論・更新Webサービス

本レポジトリは、Titanic号の生還結者を予測する機械学習モデルについて、以下二点を行うWebサービスを提供します。
- Titanic号の乗客情報を元に生還結果を予測
- 生還結果を含む正解データを追加して、機械学習モデルを更新
なお、機械学習モデルにはロジスティック回帰を使用します。

## 動作条件
Python3.11で動作確認しました。
おそらくPython3.9以降であれば動作すると思われます。

## Webサーバの実装
PythonのASGI Webサーバとして、Uvicornを使用し、Webフレームワークとしては、FastAPIを使用しました。
- Uvicorn: https://www.uvicorn.org/
- FastAPI: https://fastapi.tiangolo.com/

## 環境構築
一例として、Pythonのvenvを使用して、以下の手順で環境構築します。
```bash
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Webサービスの起動
uvicornを使用して起動します。
```bash
uvicorn --log-level trace titanicml:app
```

## APIの使用
以下に生還結果予測とモデル更新のAPIに関する説明をcurlコマンドを例に行いますが、
jupytor notebookの[request.ipynb](./request.ipynb)ではPythonによるAPI呼び出しの例も記載しています。

### データ
本サービスで使用する客員情報のデータを以下表にまとめます。
| 変数名      | データ型 | 説明 |
| -------- | ----- | -- |
| Survived | int   | 生還結果 (0: 死亡, 1: 生還)。本サービスの予測対象となる目的変数   |
| Pclass   | int   | 客室クラス (1,2,3の順に等級が上がる)   |
| Sex      | int   | 性別(0: 男性, 1: 女性)  |
| Age      | float | 年齢 |
| Fare     | float | 運賃 |


### 生還結果予測: titanic/predict
生還結果を予測するにはcurlコマンドで次のように実行できます。
```bash
curl -X 'POST' \
  'http://127.0.0.1:8000/titanic/predict' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "Pclass": [
    3,
    1
  ],
  "Sex": [
    0,
    1
  ],
  "Age": [
    22,
    50
  ],
  "Fare": [
    7.25,
    30
  ]
}'
# Response:
# {"Survived":[0, 1]}
```

### モデル更新: titanic/update
モデルを更新するにはcurlコマンドで次のように実行できます。
```bash
curl -X 'POST' \
  'http://127.0.0.1:8000/titanic/update' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '{
  "Survived": [
    0,
    1
  ],
  "Pclass": [
    3,
    1
  ],
  "Sex": [
    0,
    1
  ],
  "Age": [
    22,
    50
  ],
  "Fare": [
    7.25,
    30
  ]
}'
# Response:
# {
#   "model": {
#     "dataset": {
#       "accuracy": 0.726027397260274
#     },
#     ...snip...
```

以上