fastapi
uvicorn[standard]
aiohttp
pandas
scikit-learn
jupyter
matplotlib